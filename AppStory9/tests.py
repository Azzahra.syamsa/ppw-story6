from django.test import TestCase, Client
from .views import login_page, logout_user, landing_page, register_page
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from http import HTTPStatus

# Create your tests here.
class TestApp9(TestCase):
    #TEST URL
    def test_url_login(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        user = User.objects.create_user(username="abcde", password="1818abcde")
        user.save()
        c = Client()
        c.login(username="abcde", password="1818abcde")
        response = c.get('/story9/logout/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/story9/login/')

    def test_url_login_user_already_logged_in(self):
        user = User.objects.create_user(username="abcde", password="1818abcde")
        user.save()
        c = Client()
        c.login(username="abcde", password="1818abcde")
        response = c.get('/story9/login/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/story9/landing/')

    def test_url_login_correct_data(self):
        user = User.objects.create_user(username="abcde", password="1818abcde")
        user.save()
        response = Client().post('/story9/login/', data={
            "username": "abcde",
            "password": "1818abcde",
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/story9/landing/')

    def test_url_login_invalid_data(self):
        user = User.objects.create_user(username="abcde", password="1818abcde")
        user.save()
        response = Client().post('/story9/login/', data={
            "username": "abcde",
            "password": "1818abcdeeee",
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('Username OR password you enter is incorrect', html_contents)

    def test_url_register_correct_data(self):
        response = Client().post('/story9/register/', data={
            "username": "ayabaya",
            "email": "ayabaya@gmail.com",
            "password1": "yaya1818",
            "password2": "yaya1818"
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/story9/login/')

    def test_url_register_wrong_data(self):
        response = Client().post('/story9/register/', data={
            "username": "cacaca",
            "email": "acaca@gmail.com",
            "password1": "cacacape",
            "password2": "capepe"
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('The two password fields didn’t match.', html_contents)

    #TEST FUNCTION
    def test_func_login_page(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, login_page)

    def test_func_register_page(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, register_page)

    def test_func_logout_user(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, logout_user)

    def test_func_landing_page(self):
        found = resolve('/story9/landing/')
        self.assertEqual(found.func, landing_page)
