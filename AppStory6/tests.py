from django.test import TestCase, Client
from django.urls import resolve
from .models import Acara, Kehadiran
from .views import nambahAcara, nambahKehadiran
from .forms import form_acara, form_kehadiran

# Create your tests here.
class test_listacara(TestCase):
    def test_url_listacara_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_views_nambahKehadiran(self):
        found = resolve('/')            
        self.assertEqual(found.func, nambahKehadiran)

    def test_template_listacara(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'listacara.html')

    def test_models_create_listacara(self):
        acara1 = Acara.objects.create(acara="Akad")
        Kehadiran.objects.create(acara=acara1, nama="jahek")
        jumlah = Kehadiran.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_forms_kehadiran_valid(self):
        resepsi = Acara.objects.create(acara = "resepsi")
        form = form_kehadiran(data = {'acara' : resepsi, 'nama' : 'test'})
        self.assertTrue(form.is_valid())

    def test_views_formkehadiran(self):
        response = Client().get('/')
        isi_html = response.content.decode("utf8")
        self.assertIn('>Tambahkan Rangkaian Acara</', isi_html)
        self.assertIn('<form method="POST" action="" style="text-align: justify; padding-left: 5rem;">', isi_html)


class test_tambahacara(TestCase):
    def test_url_tambahacara_exist(self):
        response = Client().get('/tambahacara')
        self.assertEqual(response.status_code,200)

    def test_views_nambahAcara(self):
        found = resolve('/tambahacara')            
        self.assertEqual(found.func, nambahAcara)

    def test_template_tambahacara(self):
        response = Client().get('/tambahacara')
        self.assertTemplateUsed(response, 'tambahacara.html')

    def test_models_create_tambahacara(self):
        Acara.objects.create(acara="Resepsi")
        jumlah = Acara.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_forms_acara_valid(self):
        form = form_acara(data={'acara': 'akad'})
        self.assertTrue(form.is_valid())

    def test_views_formacara(self):
        response = Client().get("/tambahacara")
        isi_view = response.content.decode("utf8")
        self.assertIn('>Tambahkan Rangkaian Acara</', isi_view)
        self.assertIn('<form method="POST" action="" style="text-align: justify; padding-left: 5rem;">', isi_view)