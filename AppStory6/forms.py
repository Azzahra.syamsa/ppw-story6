from django.forms import ModelForm
from .models import Acara, Kehadiran

class form_acara(ModelForm):
    class Meta:
        model = Acara
        fields = '__all__'

class form_kehadiran(ModelForm):
    class Meta:
        model = Kehadiran
        fields = '__all__'