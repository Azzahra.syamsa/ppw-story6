from django.db import models

# Create your models here.
class Acara(models.Model):
    acara = models.CharField('Acara', max_length=30, null=True, blank=False)

    def __str__(self):
        return self.acara

class Kehadiran(models.Model):
    nama = models.CharField('Nama', max_length=30, null=True, blank=False)
    acara = models.ForeignKey(Acara, on_delete=models.CASCADE, null=True, blank=False)

    def __str__(self):
        return self.nama
    