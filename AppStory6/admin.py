from django.contrib import admin
from .models import Acara, Kehadiran

# Register your models here.
admin.site.register(Acara)
admin.site.register(Kehadiran)