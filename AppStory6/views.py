from django.shortcuts import render
from .models import Acara, Kehadiran
from .forms import form_acara, form_kehadiran
from django.http import HttpResponseRedirect


# Create your views here.
def nambahAcara(request):
    form = form_acara(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/')

    data_acara = Acara.objects.all()
    return render(request, 'tambahacara.html', {'data_acara' : data_acara, 'formA' : form})


def nambahKehadiran(request):
    form = form_kehadiran(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()

    data_acara = Acara.objects.all()
    data_kehadiran = Kehadiran.objects.all()
    return render(request, 'listacara.html', {'formK' : form, 'data_kehadiran' : data_kehadiran, 'data_acara' : data_acara})
