from django.test import TestCase, Client
from .views import home_story7
from django.urls import resolve, reverse

# Create your tests here.
class TestApp7(TestCase):
    def test_url_home7(self):
        response = Client().get('/story7')
        self.assertEqual(response.status_code, 200)

    def test_func_home7(self):
        found = resolve('/story7')
        self.assertEqual(found.func, home_story7)