$(document).ready(() => {
    $('#searchButton').click( function(){
        var key = $('#search').val();
    
    $.ajax({
        method: 'GET',
        url: '/story8/getData?key=' + key +'/',
        success: function(response) {
            console.log(response);
            $('#data').empty();
            
            var result = "";
            for (let i = 0; i < response.items.length; i++) {
                result += ('<tr>' + '<td>' + response.items[i].volumeInfo.title + '</td>' + '<td>' + response.items[i].volumeInfo.authors + '</td>'
                + '<td>')

                if (response.items[i].volumeInfo.description != undefined) {
                    if (response.items[i].volumeInfo.description.length > 230) {
                        result += response.items[i].volumeInfo.description.substring(0,230) + '...' + '</td>'
                    } else {
                        result += response.items[i].volumeInfo.description + '</td>'
                    }
                } else {
                    result += "No Description Available"
                }
                
                if (response.items[i].volumeInfo.imageLinks != null) {
                    result += ('<td><img src=' + response.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>'
                    + '</tr>')
                } else {
                    result += ('<td> <p> No Image Preview Available </p> </td>'+ '</tr>')
                }
            }
            console.log(result);
            $('#data').append(result);
            $('#data').append('</table>');
        }
    })
    })
})
