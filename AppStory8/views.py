from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import requests, json

# Create your views here.
def home_story8(request):
    return render(request, 'story8.html')

def get_buku_story8(request):
    key = request.GET.get('key')
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)