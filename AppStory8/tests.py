from django.test import TestCase, Client
from .views import home_story8
from django.urls import resolve, reverse

# Create your tests here.
class TestApp8(TestCase):
    def test_url_home8(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_url_getData_is_exist(self):
        response = Client().get("/story8/getData?key=cc")
        self.assertEquals(200, response.status_code)

    def test_func_story8(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, home_story8)


