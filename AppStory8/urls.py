from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.home_story8, name='home8'),
    path('getData', views.get_buku_story8, name='data'),
]